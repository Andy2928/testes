using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public GameObject cmr;
    public GameObject player;
    void Update()
    {
        gameObject.transform.eulerAngles = new Vector3(cmr.transform.rotation.x * 120, cmr.transform.rotation.y * 120, 0);
        gameObject.transform.position = new Vector3(player.transform.position.x + 0.45f, player.transform.position.y - 0.15f, player.transform.position.z);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public GameObject WM;
    Rigidbody rb;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "WM")
        {
            WM = other.gameObject;
            rb = other.GetComponent<Rigidbody>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        WM.transform.position = new Vector3(5, 5, 21);
        WM.transform.rotation = new Quaternion(0, 47, 0, 0);
        rb.velocity = new Vector3(0, 0, 0);
    }
}
